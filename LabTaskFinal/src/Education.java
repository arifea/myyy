


/*
 * author= SP15-BSE-031
 * Muhammad Osama Jahangir
 * 
 *bnvdfd
 */
public class Education {
	private String titleOfCertificate;
	private int yearOfCompletion;
	private double score;
	
	
	
	
	public Education() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Education(String titleOfCertificate, int yearOfCompletion, double score) {
		super();
		this.titleOfCertificate = titleOfCertificate;
		this.yearOfCompletion = yearOfCompletion;
		this.score = score;
	}
	public String getTitleOfCertificate() {
		return titleOfCertificate;
	}
	public void setTitleOfCertificate(String titleOfCertificate) {
		this.titleOfCertificate = titleOfCertificate;
	}
	public int getYearOfCompletion() {
		return yearOfCompletion;
	}
	public void setYearOfCompletion(int yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Education [titleOfCertificate=" + titleOfCertificate + ", yearOfCompletion=" + yearOfCompletion
				+ ", score=" + score + "]";
	}
	
	

}
